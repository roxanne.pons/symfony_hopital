<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $capicity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hopital", inversedBy="hasRoom")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hopitalRooms;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admission", mappedBy="RoomAdmissions")
     */
    private $hasAdmissions;

    public function __construct()
    {
        $this->hasAdmissions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName() . " " . $this->getHopitalRooms();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCapicity(): ?int
    {
        return $this->capicity;
    }

    public function setCapicity(int $capicity): self
    {
        $this->capicity = $capicity;

        return $this;
    }

    public function getHopitalRooms(): ?Hopital
    {
        return $this->hopitalRooms;
    }

    public function setHopitalRooms(?Hopital $hopitalRooms): self
    {
        $this->hopitalRooms = $hopitalRooms;

        return $this;
    }

    /**
     * @return Collection|Admission[]
     */
    public function getHasAdmissions(): Collection
    {
        return $this->hasAdmissions;
    }

    public function addHasAdmission(Admission $hasAdmission): self
    {
        if (!$this->hasAdmissions->contains($hasAdmission)) {
            $this->hasAdmissions[] = $hasAdmission;
            $hasAdmission->setRoomAdmissions($this);
        }

        return $this;
    }

    public function removeHasAdmission(Admission $hasAdmission): self
    {
        if ($this->hasAdmissions->contains($hasAdmission)) {
            $this->hasAdmissions->removeElement($hasAdmission);
            // set the owning side to null (unless already changed)
            if ($hasAdmission->getRoomAdmissions() === $this) {
                $hasAdmission->setRoomAdmissions(null);
            }
        }

        return $this;
    }
}
