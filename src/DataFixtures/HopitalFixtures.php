<?php

namespace App\DataFixtures;

use App\Entity\Hopital;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class HopitalFixtures extends Fixture implements DependentFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $names = [
      ["HUG", "Rue Gentil-Bois 4", "1205", "Genève", "CH"],
      ["CHUV", " Avenue PierreFonce 3", "1217", "Lausanne", "CH"],
      ["Clinic", "Route du chemin 11", "1009", "Pully", "CH"],
      ["Ceury", "Chemin du Pont 3", "1312", "Lausanne", "CH"],
      ["HP", "Santier du Bouclier 1", "74164", "Saint-Julien", "FR"],
    ];

    foreach ($names as $name) {

      $hopital = new Hopital();
      $hopital
        ->setName($name[0])
        ->setAddress($name[1])
        ->setPostalCode($name[2])
        ->setCity($name[3])
        ->setCountry($name[4]);

      $manager->persist($hopital);
    }

    $manager->flush();
  }
  public function getDependencies()
  {
    return [AppFixtures::class];
  }
}
