<?php

namespace App\DataFixtures;

use App\Entity\Room;
use App\Repository\HopitalRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RoomFixtures extends Fixture implements DependentFixtureInterface
{
  private $hopitalRepository;

  public function __construct(HopitalRepository $hopitalRepository)
  {
    $this->hopitalRepository = $hopitalRepository;
  }

  public function load(ObjectManager $manager)
  {
    $hopitals = $this->hopitalRepository->findAll();

    foreach ($hopitals as $hopital) {
      $roomToCreate = rand(1, 10);

      for ($i = 1; $i <= $roomToCreate; $i++) {

        $room = new Room();

        $room->setName("Room $i");
        $room->setCapicity(rand(1, 8));
        $room->setHopitalRooms($hopital);

        $manager->persist($room);
      }
    }

    $manager->flush();
  }
  public function getDependencies()
  {
    return [AppFixtures::class, HopitalFixtures::class];
  }
}
