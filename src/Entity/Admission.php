<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdmissionRepository")
 */
class Admission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $EntryDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $ExitDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="hasAdmissions")
     */
    private $RoomAdmissions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="hasAdmissions")
     */
    private $PatientAdmissions;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getRoomAdmissions() . "";
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->EntryDate;
    }

    public function setEntryDate(\DateTimeInterface $EntryDate): self
    {
        $this->EntryDate = $EntryDate;

        return $this;
    }


    public function getExitDate(): ?\DateTimeInterface
    {
        return $this->ExitDate;
    }

    public function setExitDate(\DateTimeInterface $ExitDate): self
    {
        $this->ExitDate = $ExitDate;

        return $this;
    }

    public function getRoomAdmissions(): ?Room
    {
        return $this->RoomAdmissions;
    }

    public function setRoomAdmissions(?Room $RoomAdmissions): self
    {
        $this->RoomAdmissions = $RoomAdmissions;

        return $this;
    }

    public function getPatientAdmissions(): ?Patient
    {
        return $this->PatientAdmissions;
    }

    public function setPatientAdmissions(?Patient $PatientAdmissions): self
    {
        $this->PatientAdmissions = $PatientAdmissions;

        return $this;
    }
}
