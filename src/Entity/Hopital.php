<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HopitalRepository")
 */
class Hopital
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Room", mappedBy="hopitalRooms")
     */
    private $hasRoom;

    public function __construct()
    {
        $this->hasRoom = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    //pour retourner un objet en string
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Room[]
     */
    public function getHasRoom(): Collection
    {
        return $this->hasRoom;
    }

    public function addHasRoom(Room $hasRoom): self
    {
        if (!$this->hasRoom->contains($hasRoom)) {
            $this->hasRoom[] = $hasRoom;
            $hasRoom->setHopitalRooms($this);
        }

        return $this;
    }

    public function removeHasRoom(Room $hasRoom): self
    {
        if ($this->hasRoom->contains($hasRoom)) {
            $this->hasRoom->removeElement($hasRoom);
            // set the owning side to null (unless already changed)
            if ($hasRoom->getHopitalRooms() === $this) {
                $hasRoom->setHopitalRooms(null);
            }
        }

        return $this;
    }
}
