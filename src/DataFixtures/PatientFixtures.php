<?php

namespace App\DataFixtures;

use App\Entity\Patient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PatientFixtures extends Fixture implements DependentFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    $names = [
      ["Roxanne", "Pons", "12054939", "18.03.1997"],
      ["Eliane", "Rasch", "4839240", "22.06.1964"],
      ["Patrick", "Malabar", "3305503", "10.12.1927"],
      ["Albert", "Damry", "5049939", "17.04.2020"],
      ["Jérémy", "Fontenet", "4968371", "02.07.2010"],
      ["Salomé", "Elastici", "1222939", "07.12.1997"],
      ["Eric", "Morand", "4834857", "29.03.2014"],
      ["James", "Qurashi", "3838303", "30.02.1924"],
      ["Jonathan", "Purro", "293847", "22.04.1920"],
      ["Antoine", "Rudaz", "111121", "12.07.1910"],

    ];

    foreach ($names as $name) {

      $patient = new Patient();
      $patient
        ->setFirstName($name[0])
        ->setLastName($name[1])
        ->setAvsNumber($name[2])
        ->setBirthdate($name[3]);

      $manager->persist($patient);
    }

    $manager->flush();
  }
  public function getDependencies()
  {
    return [AppFixtures::class];
  }
}
