<?php

namespace App\DataFixtures;

use App\Entity\Admission;
use App\Repository\PatientRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AdmissionFixtures extends Fixture implements DependentFixtureInterface
{
    private $patientRepository;

    public function __construct(PatientRepository $patientRepository)
    {
        $this->patientRepository = $patientRepository;
    }
    public function load(ObjectManager $manager)
    {
        $patients = $this->patientRepository->findAll();

        foreach ($patients as $patient) {
            $admissionToCreate = rand(1, 5);

            for ($i = 1; $i <= $admissionToCreate; $i++) {

                $admission = new Admission();

                $admission->setEntryDate(new \DateTime('2010-1-05'));
                $admission->setExitDate(new \DateTime('2021-03-19'));
                $admission->setPatientAdmissions($patient);

                $manager->persist($admission);
            }
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [AppFixtures::class, PatientFixtures::class];
    }
}
