<?php

namespace App\Repository;

use App\Entity\PatientCrud;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PatientCrud|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatientCrud|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatientCrud[]    findAll()
 * @method PatientCrud[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientCrudRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatientCrud::class);
    }

    // /**
    //  * @return PatientCrud[] Returns an array of PatientCrud objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PatientCrud
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
