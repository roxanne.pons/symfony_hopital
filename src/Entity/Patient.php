<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastName;

    /**
     * @ORM\Column(type="integer")
     */
    private $avsNumber;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $birthdate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admission", mappedBy="PatientAdmissions")
     */
    private $hasAdmissions;

    public function __construct()
    {
        $this->hasAdmissions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAvsNumber(): ?int
    {
        return $this->avsNumber;
    }

    public function setAvsNumber(int $avsNumber): self
    {
        $this->avsNumber = $avsNumber;

        return $this;
    }

    public function getBirthdate(): ?string
    {
        return $this->birthdate;
    }

    public function setBirthdate(string $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @return Collection|Admission[]
     */
    public function getHasAdmissions(): Collection
    {
        return $this->hasAdmissions;
    }


    public function addHasAdmission(Admission $hasAdmission): self
    {
        if (!$this->hasAdmissions->contains($hasAdmission)) {
            $this->hasAdmissions[] = $hasAdmission;
            $hasAdmission->setPatientAdmissions($this);
        }

        return $this;
    }

    public function removeHasAdmission(Admission $hasAdmission): self
    {
        if ($this->hasAdmissions->contains($hasAdmission)) {
            $this->hasAdmissions->removeElement($hasAdmission);
            // set the owning side to null (unless already changed)
            if ($hasAdmission->getPatientAdmissions() === $this) {
                $hasAdmission->setPatientAdmissions(null);
            }
        }

        return $this;
    }
}
