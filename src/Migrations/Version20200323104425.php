<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323104425 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hopital (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, address VARCHAR(100) DEFAULT NULL, postal_code VARCHAR(50) DEFAULT NULL, city VARCHAR(50) NOT NULL, country VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE admission (id INT AUTO_INCREMENT NOT NULL, room_admissions_id INT DEFAULT NULL, patient_admissions_id INT DEFAULT NULL, entry_date DATETIME NOT NULL, exit_date DATETIME NOT NULL, INDEX IDX_F4BB024AE2B251EB (room_admissions_id), INDEX IDX_F4BB024A4A85AFF9 (patient_admissions_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, hopital_rooms_id INT NOT NULL, name VARCHAR(50) NOT NULL, capicity INT NOT NULL, INDEX IDX_729F519BE87192F8 (hopital_rooms_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, avs_number INT NOT NULL, birthdate VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE admission ADD CONSTRAINT FK_F4BB024AE2B251EB FOREIGN KEY (room_admissions_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE admission ADD CONSTRAINT FK_F4BB024A4A85AFF9 FOREIGN KEY (patient_admissions_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BE87192F8 FOREIGN KEY (hopital_rooms_id) REFERENCES hopital (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519BE87192F8');
        $this->addSql('ALTER TABLE admission DROP FOREIGN KEY FK_F4BB024AE2B251EB');
        $this->addSql('ALTER TABLE admission DROP FOREIGN KEY FK_F4BB024A4A85AFF9');
        $this->addSql('DROP TABLE hopital');
        $this->addSql('DROP TABLE admission');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE patient');
    }
}
